<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'admin', 'display_name' => 'Người quản trị']);
        Role::create(['name' => 'user', 'display_name' => 'Người dùng']);
        Role::create(['name' => 'store-manager', 'display_name' => 'Người quản lý kho']);
    }
}
