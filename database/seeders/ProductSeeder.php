<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Product::factory(20)->create();
        $products = Product::all();
        $categoryIdArray = range(1, 11);
        foreach ($products as $product) {
            $quantity = rand(2, 5);
            shuffle($categoryIdArray);
            for($i = 0 ; $i < $quantity; $i++) {
                $product->categories()->attach([$categoryIdArray[$i]]);
            }
        }
    }
}
