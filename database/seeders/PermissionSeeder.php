<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $showPermission = Permission::updateOrCreate(['name' => 'show-user'], ['display_name' => 'Xem user']);
        $createPermission = Permission::updateOrCreate(['name' => 'create-user'], ['display_name' => 'Tạo user']);
        $editPermission = Permission::updateOrCreate(['name' => 'edit-user'], ['display_name' => 'Sửa user']);
        $updatePermission = Permission::updateOrCreate(['name' => 'delete-user'], ['display_name' => 'Xóa user']);

        $storeManagerRole = Role::where('name', '=', 'store-manager')->first();
        if ($storeManagerRole) {
            $storeManagerRole->permissions()->attach([$showPermission->id, $createPermission->id, $editPermission->id,
                $updatePermission->id]);
        }
    }
}
