<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'image' => $this->faker->image(storage_path('app/public/images'), 640, 480, null, false),
            'description' => $this->faker->text,
            'quantity' => $this->faker->numberBetween(100, 1000),
            'price' => $this->faker->randomFloat(2, 0, 10000)
        ];
    }
}
