<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_category_if_category_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $category = Category::factory()->create();
        $response = $this->delete(route('categories.destroy', $category->id));

        $this->assertDatabaseMissing('categories', $category->toArray());
        $response->assertRedirect(route('categories.index'));
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->delete(route('categories.destroy', $category->id));

        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_not_delete_category_if_category_not_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $categoryId = -1;
        $response = $this->delete(route('categories.destroy', $categoryId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
