<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_category_if_category_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $category = Category::first();
        $response = $this->get(route('categories.show', $category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('category.show');
        $response->assertSee($category->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_category()
    {
        $category = Category::first();
        $response = $this->get(route('categories.show', $category->id));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_not_get_category_if_category_not_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $categoryId = -1;
        $response = $this->get(route('categories.show', $categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
