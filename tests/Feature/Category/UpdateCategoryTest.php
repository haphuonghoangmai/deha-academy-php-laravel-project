<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_see_update_category_view_if_category_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $category = Category::first();
        $response = $this->get(route('categories.edit', $category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('category.edit');
        $response->assertSee($category->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_update_category_view()
    {
        $category = Category::first();
        $response = $this->get(route('categories.edit', $category->id));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_not_see_update_category_view_if_category_not_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $categoryId = -1;
        $response = $this->get(route('categories.edit', $categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_user_can_update_category_if_category_exits_and_data_valid()
    {
        $user = User::first();
        $this->actingAs($user);
        $category = Category::first();
        $data = [
            'name' => $this->faker->name
        ];
        $response = $this->put(route('categories.update', $category->id), $data);
        $this->assertDatabaseHas('categories', [
            'id' => $category->id,
            'name' => $data['name'],
        ]);
        $response->assertRedirect(route('categories.edit', $category->id));
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_name_is_null()
    {
        $user = User::first();
        $this->actingAs($user);
        $category = Category::first();
        $data = [
            'name' => null
        ];
        $response = $this->from(route('categories.edit', $category->id))->put(route('categories.update', $category->id), $data);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect(route('categories.edit', $category->id));
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_category_not_exits()
    {
        $user = User::first();
        $this->actingAs($user);
        $categoryId = -1;
        $data = [
            'name' => $this->faker->name
        ];
        $response = $this->put(route('categories.update', $categoryId), $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
