<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_see_create_category_view()
    {
        $user = User::first();
        $this->actingAs($user);
        $response = $this->get(route('categories.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('category.create');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_category_view()
    {
        $response = $this->get(route('categories.create'));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_create_new_node_category_if_data_valid()
    {
        $user = User::first();
        $this->actingAs($user);
        $countBefore = Category::count();
        $data = [
            'name' => $this->faker->name
        ];
        $response = $this->post(route('categories.store'), $data);
        $countAfter = Category::count();
        $this->assertEquals($countBefore + 1, $countAfter);
        $this->assertDatabaseHas('categories', [
            'name' => $data['name'],
            'parent_id' => null
        ]);
        $response->assertRedirect(route('categories.index'));
    }

    /** @test */
    public function authenticated_user_can_create_new_child_node_category_if_data_valid()
    {
        $user = User::first();
        $this->actingAs($user);
        $category = Category::first();
        $countBefore = Category::count();
        $data = [
            'name' => $this->faker->name,
            'parent-id' => $category->id
        ];
        $response = $this->post(route('categories.store'), $data);
        $countAfter = Category::count();
        $this->assertEquals($countBefore + 1, $countAfter);
        $this->assertDatabaseHas('categories', [
            'name' => $data['name'],
            'parent_id' => $category->id
        ]);
        $response->assertRedirect(route('categories.index'));
    }

    /** @test */
    public function authenticated_user_can_not_create_new_node_category_if_name_is_null()
    {
        $user = User::first();
        $this->actingAs($user);
        $data = [
            'name' => null
        ];
        $response = $this->from(route('categories.create'))->post(route('categories.store'), $data);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect(route('categories.create'));
    }
}
