<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_category()
    {
        $user = User::first();
        $this->actingAs($user);
        $category = Category::first();
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('category.index');
        $response->assertSee($category->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_category()
    {
        $response = $this->get(route('categories.index'));
        $response->assertRedirect(route('login'));
    }
}
