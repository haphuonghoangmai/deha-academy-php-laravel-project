<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_product()
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::latest('id')->first();
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee([$product->name, $product->quantity, $product->price, $product->description,
            $product->image, $product->categories[0]->name]);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_category()
    {
        $response = $this->get(route('products.index'));
        $response->assertRedirect(route('login'));
    }
}
