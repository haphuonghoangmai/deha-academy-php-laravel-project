<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class SearchProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_search_product_by_name()
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $data = [
            'q' => $product->name
        ];
        $response = $this->get(route('products.index'), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee([$product->name]);
    }

    /** @test */
    public function authenticated_user_can_search_product_by_price()
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $data = [
            'q' => $product->price
        ];
        $response = $this->get(route('products.index'), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee([$product->name]);
    }

    /** @test */
    public function authenticated_user_can_search_product_by_category()
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $category = Category::first();
        $product->categories()->attach($category->id);
        $data = [
            'q' => $category->name
        ];
        $response = $this->get(route('products.index'), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee([$product->name]);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_category()
    {
        $response = $this->get(route('products.index'));
        $response->assertRedirect(route('login'));
    }
}
