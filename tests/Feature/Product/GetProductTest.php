<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_product_if_product_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::first();
        $response = $this->get(route('products.show', $product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.show');
        $response->assertSee([$product->name, $product->quantity, $product->price, $product->description,
            $product->image, $product->categories[0]->name]);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_product()
    {
        $product = Product::first();
        $response = $this->get(route('categories.show', $product->id));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_not_get_product_if_product_not_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $productId = -1;
        $response = $this->get(route('categories.show', $productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
