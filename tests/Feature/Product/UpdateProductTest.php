<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    protected function testFailData(array $params, array $errors)
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::first();
        $category = Category::first();
        $defaults = array(
            'name' => $this->faker->name,
            'image' => UploadedFile::fake()->image('product.jpg'),
            'description' => $this->faker->text,
            'quantity' => $this->faker->numberBetween(100, 1000),
            'price' => $this->faker->randomFloat(2, 0, 10000),
            'categories' => [$category->id]
        );
        $data = array_merge($defaults, $params);
        $response =
            $this->from(route('products.edit', $product->id))->put(route('products.update', $product->id), $data);
        $response->assertSessionHasErrors($errors);
        $response->assertRedirect(route('products.edit', $product->id));
    }

    /** @test */
    public function authenticated_user_can_see_update_product_view_if_product_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::first();
        $response = $this->get(route('products.edit', $product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.edit');
        $response->assertSee([$product->name, $product->quantity, $product->price, $product->description,
            $product->image, $product->categories[0]->name]);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_update_product_view()
    {
        $product = Product::first();
        $response = $this->get(route('products.edit', $product->id));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_not_see_update_product_view_if_product_not_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $productId = -1;
        $response = $this->get(route('products.edit', $productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_user_can_update_product_if_product_exits_and_data_valid()
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::first();
        $category = Category::first();
        $data = [
            'name' => $this->faker->name,
            'image' => UploadedFile::fake()->image('product.jpg'),
            'description' => $this->faker->text,
            'quantity' => $this->faker->numberBetween(100, 1000),
            'price' => $this->faker->randomFloat(2, 0, 10000),
            'categories' => [$product->id]
        ];
        $response = $this->put(route('products.update', $product->id), $data);
        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'name' => $data['name'],
            'image' => time() . 'product.jpg',
            'description' => $data['description'],
            'quantity' => $data['quantity'],
            'price' => $data['price'],
        ]);
        $this->assertDatabaseHas('category_product', [
            'product_id' => $product->id,
            'category_id' => $category->id
        ]);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status', Response::HTTP_NO_CONTENT)->etc());
        Storage::disk('public')->assertMissing('/images/' . $product->image);
        Storage::disk('public')->assertExists('/images/' . time() . 'product.jpg');
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_name_is_null()
    {
        $this->testFailData(['name' => null], ['name']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_description_is_null()
    {
        $this->testFailData(['description' => null], ['description']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_quantity_is_null()
    {
        $this->testFailData(['quantity' => null], ['quantity']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_quantity_is_not_an_int_number()
    {
        $this->testFailData(['quantity' => 1.1], ['quantity']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_quantity_under_0()
    {
        $this->testFailData(['quantity' => -1], ['quantity']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_quantity_over_99999()
    {
        $this->testFailData(['quantity' => 100000], ['quantity']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_price_is_null()
    {
        $this->testFailData(['price' => null], ['price']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_price_is_not_an_float_number()
    {
        $this->testFailData(['price' => 'abc'], ['price']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_price_under_0()
    {
        $this->testFailData(['price' => -1], ['price']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_price_over_9999999()
    {
        $this->testFailData(['price' => 10000000], ['price']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_categories_is_null()
    {
        $this->testFailData(['categories' => null], ['categories']);
    }

    /** @test */
    public function authenticated_user_can_not_update_product_if_data_is_null()
    {
        $this->testFailData([
            'name' => null,
            'description' => null,
            'quantity' => null,
            'price' => null,
            'categories' => null
        ], [
            'name',
            'description',
            'quantity',
            'price',
            'categories'
        ]);
    }
}
