<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_product_if_product_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $product = Product::factory()->create();
        $category = Category::first();
        $product->categories()->attach($category->id);
        $response = $this->delete(route('products.destroy', $product->id));

        $this->assertDatabaseMissing('products', $product->toArray());
        $this->assertDatabaseMissing('category_product', [
            'product_id' => $product->id,
            'category_id' => $category->id
        ]);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_product()
    {
        $product = Product::first();
        $response = $this->delete(route('products.destroy', $product->id));

        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_not_delete_product_if_product_not_exists()
    {
        $user = User::first();
        $this->actingAs($user);
        $productId = -1;
        $response = $this->delete(route('products.destroy', $productId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
