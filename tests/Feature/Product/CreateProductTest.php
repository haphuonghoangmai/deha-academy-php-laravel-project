<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    protected function testFailData(array $params, array $errors)
    {
        $user = User::first();
        $this->actingAs($user);
        $category = Category::first();
        $defaults = [
            'name' => $this->faker->name,
            'image' => UploadedFile::fake()->image('product.jpg'),
            'description' => $this->faker->text,
            'quantity' => $this->faker->numberBetween(100, 1000),
            'price' => $this->faker->randomFloat(2, 0, 10000),
            'categories' => [$category->id]
        ];
        $data = array_merge($defaults, $params);
        $response = $this->from(route('products.create'))->post(route('products.store'), $data);
        $response->assertSessionHasErrors($errors);
        $response->assertRedirect(route('products.create'));
    }

    /** @test */
    public function authenticated_user_can_see_create_product_view()
    {
        $user = User::first();
        $this->actingAs($user);
        $response = $this->get(route('products.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.create');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_product_view()
    {
        $response = $this->get(route('products.create'));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_can_create_new_product_if_data_valid()
    {
        $user = User::first();
        $this->actingAs($user);
        $countBefore = Product::count();
        $category = Category::first();
        $data = [
            'name' => $this->faker->name,
            'image' => UploadedFile::fake()->image('product.jpg'),
            'description' => $this->faker->text,
            'quantity' => $this->faker->numberBetween(100, 1000),
            'price' => $this->faker->randomFloat(2, 0, 10000),
            'categories' => [$category->id]
        ];
        $response = $this->post(route('products.store'), $data);
        $countAfter = Product::count();
        $this->assertEquals($countBefore + 1, $countAfter);
        $this->assertDatabaseHas('products', [
            'name' => $data['name'],
            'image' => time().'product.jpg',
            'description' => $data['description'],
            'quantity' => $data['quantity'],
            'price' => $data['price'],
        ]);
        $this->assertDatabaseHas('category_product', [
            'product_id' => Product::latest('id')->first()->id,
            'category_id' => $category->id
        ]);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status', Response::HTTP_CREATED)->etc());
        Storage::disk('public')->assertExists('/images/'.time().'product.jpg');
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_name_is_null()
    {
        $this->testFailData(['name' => null], ['name']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_image_is_null()
    {
        $this->testFailData(['image' => null], ['image']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_image_is_not_a_file()
    {
        $this->testFailData(['image' => 'test'], ['image']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_description_is_null()
    {
        $this->testFailData(['description' => null], ['description']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_quantity_is_null()
    {
        $this->testFailData(['quantity' => null], ['quantity']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_quantity_is_not_an_int_number()
    {
        $this->testFailData(['quantity' => 1.1], ['quantity']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_quantity_under_0()
    {
        $this->testFailData(['quantity' => -1], ['quantity']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_quantity_over_99999()
    {
        $this->testFailData(['quantity' => 100000], ['quantity']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_price_is_null()
    {
        $this->testFailData(['price' => null], ['price']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_price_is_not_an_float_number()
    {
        $this->testFailData(['price' => 'abc'], ['price']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_price_under_0()
    {
        $this->testFailData(['price' => -1], ['price']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_price_over_9999999()
    {
        $this->testFailData(['price' => 10000000], ['price']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_categories_is_null()
    {
        $this->testFailData(['categories' => null], ['categories']);
    }

    /** @test */
    public function authenticated_user_can_not_create_new_product_if_data_is_null()
    {
        $this->testFailData([
            'name' => null,
            'description' => null,
            'quantity' => null,
            'price' => null,
            'categories' => null
        ], [
            'name',
            'description',
            'quantity',
            'price',
            'categories'
        ]);
    }
}
