@extends('layouts.app', ['activePage' => 'product', 'titlePage' => __('Product')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <h1 class="mb-4">{{$product->name}}</h1>
            <div class="product-item row">
                <div class="col-md-2">
                    <img src="{{asset($product->image_url)}}" alt="Product image" class="rounded">
                </div>
                <div class="product-info col-md-10">
                    <div class="row mb-2">
                        <div class="col-md-2 text-right">Quantity: </div>
                        <div class="col-md-10">{{$product->quantity}}</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2 text-right">Price: </div>
                        <div class="col-md-10">{{$product->price}}</div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-2 text-right">Description: </div>
                        <div class="col-md-10">{{$product->description}}</div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-2 text-right">Categories: </div>
                        <div class="col-md-10">
                            @foreach($product->categories as $category)
                                <span class="py-2 px-4 rounded bg-dark text-white mr-2">
                                    {{$category->name}}
                                </span>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

