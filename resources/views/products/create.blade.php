@extends('layouts.app', ['activePage' => 'product', 'titlePage' => __('Create Product')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('products.store') }}" autocomplete="off"
                          class="form-horizontal">
                        @csrf
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Create Product') }}</h4>
                            </div>
                            <div class="card-body ">
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" id="input-name" type="text"
                                                   placeholder="{{ __('Name') }}" value="{{ old('name') }}"
                                                   required="true" aria-required="true"/>
                                            <span id="name-error" class="error text-danger"
                                                  for="input-name"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Quantity') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('quantity') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
                                                name="quantity" id="input-quantity" type="number"
                                                placeholder="{{ __('Quantity') }}" value="{{ old('quantity') }}"
                                                required="true" aria-required="true"/>
                                            <span id="quantity-error" class="error text-danger"
                                                  for="input-quantity"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Price') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                                                   name="price" id="input-price" type="number"
                                                   placeholder="{{ __('Price') }}" value="{{ old('price') }}"
                                                   required="true" aria-required="true"/>
                                            <span id="price-error" class="error text-danger"
                                                  for="input-price"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Description') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                            <textarea name="description" id="input-description" cols="30" rows="10"
                                                      placeholder="{{ __('Description') }}"
                                                      class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                                            >{{old('description')}}</textarea>
                                            <span id="description-error" class="error text-danger"
                                                  for="input-description"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Category') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('category') ? ' has-danger' : '' }}">
                                            <ul class="category-list list-unstyled">
                                            </ul>
                                            <select name="category" id="input-category"
                                                    class="form-control category-select">
                                                <option value="0">Select category</option>
                                                @foreach($categories as $category)
                                                    <option class="category-option"
                                                            value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            <span id="category-error" class="error text-danger"
                                                  for="input-category"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Image') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('image') ? ' has-danger' : '' }}">
                                            <input name="image" id="input-image" type="file" accept="image/*"/>
                                            <label for="input-image" class="btn btn-outline-warning mr-4">
                                                Upload file
                                            </label>
                                            <img
                                                src="https://media.istockphoto.com/vectors/no-image-available-icon-vector-id1216251206?b=1&k=20&m=1216251206&s=170667a&w=0&h=z0hxu_BaI_tuMjMneE_APbnx_-R2KGPXgDjdwLw5W7o="
                                                alt="Product image" class="product-img rounded"
                                                width="150"
                                                height="150"
                                            >
                                            <span id="image-error" class="error text-danger"
                                                  for="input-image"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary create-btn">{{ __('Create') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script src="{{asset('/js/product.js')}}"></script>
    @endpush
@endsection
