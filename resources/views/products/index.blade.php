@extends('layouts.app', ['activePage' => 'product', 'titlePage' => __('Product List')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <a href="{{route('products.create')}}"
               class="d-inline-flex justify-content-center align-items-center mb-4 rounded bg-dark text-white py-2 px-4">
                <i class="fa-solid fa-plus mr-2"></i>
                New Product
            </a>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Product list</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="text-primary">
                                    <tr class="text-center">
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Categories</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr class="text-center product-item" data-id="{{$product->id}}">
                                            <td><a href="{{route('products.show', $product->id)}}">{{$product->id}}</a>
                                            </td>
                                            <td>
{{--                                                <img src="{{asset('images/' . $product->image)}}"--}}
                                                <img src="{{asset($product->image_url)}}"
                                                     alt="Product image"
                                                     class="rounded"
                                                     width="100"
                                                     height="100"
                                                >
                                            </td>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->description}}</td>
                                            <td>{{$product->quantity}}</td>
                                            <td>{{$product->price}}</td>
                                            <td>
                                                @foreach($product->categories as $category)
                                                    <span class="p-2 rounded bg-dark text-white mr-2 mb-2 d-inline-block">
                                                        {{$category->name}}
                                                    </span>
                                                @endforeach
                                            </td>
                                            <td class="text-right">
                                                <a href="{{route('products.edit', $product->id)}}"
                                                   class="btn btn-primary">Edit</a>
                                                <button type="button" class="btn modal-delete-btn">
                                                    DELETE
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                {{$products->links()}}
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">CONFIRM DELETE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-left">
                    Are you sure you want to delete?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary delete-btn">Delete</button>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script src="{{asset('/js/product.js')}}"></script>
    @endpush
@endsection
