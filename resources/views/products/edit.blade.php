@extends('layouts.app', ['activePage' => 'product', 'titlePage' => __('Create Product')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('products.update', $product->id) }}" autocomplete="off"
                          class="form-horizontal">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="{{$product->id}}">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Update Product') }}</h4>
                            </div>
                            <div class="card-body ">
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" id="input-name" type="text"
                                                   placeholder="{{ __('Name') }}" value="{{ $product->name }}"
                                                   required="true" aria-required="true"/>
                                            <span id="name-error" class="error text-danger"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Quantity') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('quantity') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
                                                name="quantity" id="input-quantity" type="number"
                                                placeholder="{{ __('Quantity') }}" value="{{ $product->quantity }}"
                                                required="true" aria-required="true"/>
                                            <span id="quantity-error" class="error text-danger"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Price') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                                                   name="price" id="input-price" type="number"
                                                   placeholder="{{ __('Price') }}" value="{{ $product->price  }}"
                                                   required="true" aria-required="true"/>
                                            <span id="price-error" class="error text-danger"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Description') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                            <textarea name="description" id="input-description" cols="30" rows="10"
                                                      placeholder="{{ __('Description') }}"
                                                      class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                                            >{{$product->description}}</textarea>
                                            <span id="description-error" class="error text-danger"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Category') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('category') ? ' has-danger' : '' }}">
                                            <ul class="category-list list-unstyled">
                                                @foreach($product->categories as $category)
                                                    <li class="category-item p-2 rounded bg-dark text-white d-inline-block mr-2 mb-2" data-id="{{$category->id}}">
                                                        <span class="mr-2">{{$category->name}}</span>
                                                        <i class="fa-solid fa-circle-xmark text-danger remove-category-btn"></i>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <select name="category" id="input-category"
                                                    class="form-control category-select">
                                                <option value="0">Select category</option>
                                                @foreach($categories as $category)
                                                    <option class="category-option"
                                                            value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            <span id="category-error" class="error text-danger"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Image') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('image') ? ' has-danger' : '' }}">
                                            <input name="image" id="input-image" type="file" accept="image/*"/>
                                            <label for="input-image" class="btn btn-outline-warning mr-4">
                                                Upload file
                                            </label>
                                            <img src="{{asset($product->image_url)}}"
                                                alt="Product image" class="product-img rounded"
                                                width="150"
                                                height="150"
                                            >
                                            <span id="image-error" class="error text-danger"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary update-btn">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script src="{{asset('/js/product.js')}}"></script>
    @endpush
@endsection
