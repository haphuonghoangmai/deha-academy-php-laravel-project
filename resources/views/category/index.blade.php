@extends('layouts.app', ['activePage' => 'category', 'titlePage' => __('Category List')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <a href="{{route('categories.create')}}"
               class="d-inline-flex justify-content-center align-items-center mb-4 rounded bg-dark text-white py-2 px-4">
                <i class="fa-solid fa-plus mr-2"></i>
                New Category
            </a>
            <ul class="category-list list-unstyled">
                @foreach($categories as $category)
                    <x-category-item :category="$category"></x-category-item>
                @endforeach
            </ul>
        </div>
    </div>
    @push('js')
        <script src="{{asset('js/category.js')}}"></script>
    @endpush
@endsection
