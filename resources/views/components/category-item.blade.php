@props(['category'])

<li class="category-item ml-4">
    <div class="category-handle d-inline-block">
        @if(count($category->children) != 0)
            <i class="fa-solid fa-minus mr-2"></i>
        @endif
        <span class="d-inline-block mb-2 mr-2">
            {{$category->name}}
        </span>
        <a href="{{route('categories.edit', $category->id)}}">
            <i class="fa-solid fa-pen text-dark mr-2"></i>
        </a>
        <form action="{{route('categories.destroy', $category->id)}}" method="POST" class="d-inline">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-link p-0">
                <i class="fa-solid fa-xmark text-danger"></i>
            </button>
        </form>
    </div>
    @if(count($category->children) != 0)
        <ul class="category-list list-unstyled">
            @foreach($category->children as $child)
                <x-category-item :category="$child"></x-category-item>
            @endforeach
        </ul>
    @endif
</li>

