@extends('layouts.app', ['activePage' => 'role', 'titlePage' => __('Edit Role')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('roles.update', $role->id) }}" autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Edit Role') }}</h4>
                            </div>
                            <div class="card-body ">
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Name') }}" value="{{ $role->name }}" required="true" aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <label class="col-sm-2 col-form-label">{{ __('Display name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('display_name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('display_name') ? ' is-invalid' : '' }}" name="display_name" id="input-display-name" type="text" placeholder="{{ __('Display name') }}" value="{{ $role->display_name }}" required="true" aria-required="true"/>
                                            @if ($errors->has('display_name'))
                                                <span id="display-name-error" class="error text-danger" for="input-display-name">{{ $errors->first('display_name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Permissions') }}</h4>
                        </div>
                        <div class="card-body">
                            @foreach($role->permissions as $permission)
                                <span class="btn btn-outline-primary mr-4">{{$permission->name}}</span>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
