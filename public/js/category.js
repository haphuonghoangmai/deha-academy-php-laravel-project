// const categoryList = document.querySelectorAll('.category-list');
const categoryItem = document.querySelectorAll('.category-item');

categoryItem.forEach(item => {
    item.onclick = (e) => {
        e.stopPropagation();
        const categoryHandle = e.target.closest('.category-handle');
        if (categoryHandle) {
            const categorylist = item.querySelector('.category-list');
            if (categorylist) {
                let isOpen = categorylist.style.display !== 'none';
                const openIcon = categoryHandle.querySelector('i');
                if (isOpen) {
                    categorylist.style.display = 'none';
                    openIcon.classList.remove('fa-minus');
                    openIcon.classList.add('fa-plus');
                }
                else {
                    categorylist.style.display = 'block';
                    openIcon.classList.remove('fa-plus');
                    openIcon.classList.add('fa-minus');
                }
            }
        }
    }
})
