const categorySelect = $('.category-select');
const categoryOptions = $('.category-option');
const categoryListUl = $('.category-list');
const productInputFile = $('#input-image');
const productImg = $('.product-img');
const createBtn = $('.create-btn');
const updateBtn = $('.update-btn');
const deleteBtn = $('.delete-btn');
const modalDeleteBtns = $('.modal-delete-btn');
const searchInput = $('.search-input');
const searchResultList = $('.search-result-list');
const nameInput = $('input[name="name"]');
const quantityInput = $('input[name="quantity"]');
const priceInput = $('input[name="price"]');
const descriptionInput = $('textarea[name="description"]');

const Product = (function () {
    let modules = {
        categoryList: [],
        productId: null
    };

    modules.setupAjax = function () {
        const csrfToken = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
    }

    modules.setupCategoryList = function () {
        if (categoryListUl) {
            const categoryItems = $(".category-item");
            categoryItems.each((index, category) => {
                modules.categoryList.push(category.dataset.id);
            })
        }
    }

    modules.setProductId = function (id) {
        modules.productId = id;
    }

    modules.addCategory = function (categoryItem) {
        const value = categoryItem.value;
        const text = categoryItem.innerText;
        if (!modules.categoryList.includes(value)) {
            const item = `<li class="category-item p-2 rounded bg-dark text-white d-inline-block mr-2 mb-2" data-id="${value}">
                <span class="mr-2">${text}</span>
                <i class="fa-solid fa-circle-xmark text-danger remove-category-btn"></i>
            </li>`;
            categoryListUl.append(item);
            modules.categoryList.push(value);
            $('#category-error').text('');
        }
    }

    modules.removeCategory = function (categoryItem) {
        const value = categoryItem.dataset.id;
        if (modules.categoryList.includes(value)) {
            categoryItem.remove();
            const index = modules.categoryList.indexOf(value);
            modules.categoryList.splice(index, 1);
        }
    }

    modules.handleCreateAndUpdate = function (type = 'CREATE') {
        const formData = new FormData();
        const name = nameInput.val().trim();
        const quantity = quantityInput.val();
        const price = priceInput.val();
        const description = descriptionInput.val().trim();
        const categories = modules.categoryList;
        const image = $('input[name="image"]')[0].files[0];
        formData.append('name', name);
        formData.append('quantity', quantity);
        formData.append('price', price);
        formData.append('description', description);
        formData.append('image', image);
        categories.forEach(category => {
            formData.append('categories[]', category);
        })
        if (type === 'UPDATE')
            formData.append('_method', 'PUT');
        $.ajax({
            type: 'POST',
            url: type === 'CREATE' ? '/products' : `/products/${modules.productId}`,
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                if (data.status === 201 || data.status === 204) {
                    if ($('.alert-success').length === 0) {
                        const alert = `
                                <div class="alert alert-success alert-dismissible position-fixed"
                                     style="top: 30px; right: 0; min-width: 400px; z-index: 9999">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <span>
                                        <strong>Success!</strong> ${type === 'CREATE' ? 'Product created' : 'Product updated'}
                                    </span>
                                </div>
                        `
                        $('.content').append(alert);
                    }
                }
            },
            error: function (err) {
                if (err.status === 422) {
                    const message = err.responseJSON.errors;
                    $('#name-error').text(message['name'] && message['name'][0]);
                    $('#quantity-error').text(message['quantity'] && message['quantity'][0]);
                    $('#price-error').text(message['price'] && message['price'][0]);
                    $('#description-error').text(message['description'] && message['description'][0]);
                    $('#category-error').text(message['categories'] && message['categories'][0]);
                    if (type === 'CREATE')
                        $('#image-error').text(message['image'] && message['image'][0]);
                } else {
                    if ($('.alert-danger').length === 0) {
                        const alert = `
                                <div class="alert alert-danger alert-dismissible position-fixed"
                                     style="top: 30px; right: 0; min-width: 400px; z-index: 9999">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <span>
                                        <strong>Fail!</strong> ${type === 'CREATE' ? 'Cannot create product' : 'Cannot update product'}
                                    </span>
                                </div>
                        `
                        $('.content').append(alert);
                    }
                }
            }
        })
    }

    modules.createProduct = function () {
        modules.handleCreateAndUpdate();
    }

    modules.updateProduct = function () {
        modules.productId = $('input[name="id"]').val();
        modules.handleCreateAndUpdate('UPDATE');
    }

    modules.deleteProduct = function () {
        $.ajax({
            type: 'DELETE',
            url: `/products/${modules.productId}`,
            success: function (data) {
                $('#deleteModal').modal('hide');
                const productItem = $(`.product-item[data-id="${modules.productId}"]`);
                productItem.remove();
            }
        })
    }

    modules.searchProduct = function (q) {
        $.ajax({
            type: 'GET',
            url: '/products/search',
            data: {
                q: q.trim()
            },
            success: function (data) {
                console.log(data);
                searchResultList.empty();
                const html = data.data.map(product => {
                    return `<li class="search-result-item w-100 border-bottom p-2">
                        <a href="/products/${product.id}" class="d-flex align-items-center text-dark">
                            <img src="images/${product.image}" alt="Product image" width="40" height="40" class="rounded mr-2">
                            <span>${product.name}</span>
                        </a>
                    </li>`
                }).join('');
                searchResultList.append(html);
            }
        })
    }

    return modules;
})(window.jQuery, window, document);

$(document).ready(function () {
    Product.setupAjax();
    Product.setupCategoryList();

    if (categorySelect) {
        categorySelect.on("change", (e) => {
            const value = e.target.value;
            if (value !== 0) {
                const categoryItem = Array.from(categoryOptions).find(option => option.value === value);
                Product.addCategory(categoryItem);
            }
        })
    }

    if (categoryListUl) {
        categoryListUl.on("click", (e) => {
            const removeBtn = e.target.closest('.remove-category-btn');
            if (removeBtn) {
                const categoryItem = removeBtn.closest('.category-item');
                Product.removeCategory(categoryItem);
            }
        })
    }

    if (productInputFile) {
        productInputFile.on("change", (e) => {
            const file = e.target.files[0];
            const url = URL.createObjectURL(file);
            productImg.attr("src", url);
            $('#image-error').text('');
        })
    }

    if (createBtn) {
        createBtn.on("click", (e) => {
            e.preventDefault();
            Product.createProduct();
        })
    }

    if (updateBtn) {
        updateBtn.on("click", (e) => {
            e.preventDefault();
            Product.updateProduct();
        })
    }

    if (modalDeleteBtns) {
        modalDeleteBtns.on("click", (e) => {
            $("#deleteModal").modal('show');
            const productId = e.target.closest('.product-item').dataset.id;
            Product.setProductId(productId);
        })
    }

    if (deleteBtn) {
        deleteBtn.on("click", () => {
            Product.deleteProduct();
        })
    }

    if (searchInput) {
        searchInput.on(
        {
            "keyup": _.debounce((e) => {
                if (e.keyCode === 13) {
                    window.location = `${window.origin}/products?q=${searchInput.val().trim()}`;
                } else
                    Product.searchProduct(searchInput.val());
            }, 500),
            "blur": (e) => {
                if (!(e.relatedTarget && e.relatedTarget.closest('.search-result-item')))
                    searchResultList.empty();
            }
        })
    }

    if (nameInput) {
        nameInput.on("change", () => {
            if (nameInput.val().trim() !== '')
                $('#name-error').text('');
        })
    }

    if (quantityInput) {
        quantityInput.on("change", () => {
            if (quantityInput.val().trim() !== '')
                $('#quantity-error').text('');
        })
    }

    if (priceInput) {
        priceInput.on("change", () => {
            if (priceInput.val().trim() !== '')
                $('#price-error').text('');
        })
    }

    if (descriptionInput) {
        descriptionInput.on("keyup", () => {
            if (descriptionInput.val().trim() !== '')
                $('#description-error').text('');
        })
    }
});

