<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Image;

trait HandleImage
{
    function storeImage($image)
    {
        $thumbnailImage = Image::make($image);
        if (!Storage::disk('public')->exists('images')) {
            Storage::disk('public')->makeDirectory('images');
        }
        $thumbnailPath = storage_path('app/public/images/');
        $thumbnailImage->resize(150, 150);
        $fileName = time().$image->getClientOriginalName();
        $location = $thumbnailPath.$fileName;
        $thumbnailImage->save($location);

        return $fileName;
    }

    function deleteImage($imageUrl)
    {
        if (File::exists(storage_path('app/public/images/').$imageUrl)) {
            File::delete(storage_path('app/public/images/').$imageUrl);

            return true;
        }

        return false;
    }
}
