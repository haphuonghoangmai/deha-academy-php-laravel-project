<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    public function model()
    {
        // TODO: Implement model() method.
        return Product::class;
    }

    public function addCategories(Product $product, array $categories)
    {
        // TODO: Implement addCategories() method.
        $product->categories()->attach($categories);
    }

    public function removeCategories(Product $product, array $categories = null)
    {
        // TODO: Implement removeCategories() method.
        $product->categories()->detach($categories);
    }

    public function search($searchTerm = null, $limit = 5)
    {
        // TODO: Implement search() method.
        return Product::search($searchTerm, $limit);
    }
}
