<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\RepositoryInterface;

interface ProductRepositoryInterface extends RepositoryInterface
{
    /**
     * Attach category
     * @param Product $product
     * @param array $categories
     * @return mixed
     */
    public function addCategories(Product $product, array $categories);

    /**
     * Detach category
     * @param Product $product
     * @param array $categories
     * @return mixed
     */
    public function removeCategories(Product $product, array $categories = null);

    /**
     * Search product
     * @param $searchTerm
     * @return mixed
     */
    public function search($searchTerm = null, $limit = 5);
}
