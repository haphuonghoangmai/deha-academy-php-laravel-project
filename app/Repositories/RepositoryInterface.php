<?php

namespace App\Repositories;

interface RepositoryInterface
{
    /**
     * Get all
     * @return mixed
     */
    public function all();

    /**
     * Paginate
     * @param null $limit
     * @param array $columns
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*']);

    /**
     * Get one
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*']);

    public function findWithoutRedirect($id, $columns = ['*']);

    public function findOrFail($id, $columns = ['*']);

    public function findOrFailWithTrashed($id, $columns = ['*']);

    /**
     * Create
     * @param array $input
     * @return mixed
     */
    public function create(array $input);

    /**
     * Update
     * @param array $input
     * @param $id
     * @return mixed
     */
    public function update(array $input, $id);

    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * Multiple delete
     * @param array $ids
     * @return mixed
     */
    public function multipleDelete(array $ids);

    /**
     * Get latest
     * @param $col
     * @return mixed
     */
    public function latest($col);

    /**
     * Update or create
     * @param array $arrayFind
     * @param $arrayCreate
     * @return mixed
     */
    public function updateOrCreate(array $arrayFind, $arrayCreate = ['*']);

    /**
     * Insert
     * @param $data
     * @return mixed
     */
    public function insertMany($data);

    public function with(array $relations);

    public function get();
}
