<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'image',
        'description',
        'quantity',
        'price'
    ];

    public function scopeSearch($query, $searchTerm, $limit = null)
    {
        $searchValue = '%' . $searchTerm . '%';
        return $query->with('categories')
            ->where('name', 'like', $searchValue)
            ->orWhere('price', 'like', $searchValue)
            ->orWhereHas('categories', function ($query) use ($searchValue) {
                $query->where('name', 'like', $searchValue);
            })
            ->paginate($limit);
    }

    public function getImageUrlAttribute()
    {
        return 'storage/images/'. (Storage::disk('public')->exists('/images/'.$this->image) ? $this->image : 'noImage.jpg');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
