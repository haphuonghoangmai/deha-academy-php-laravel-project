<?php

namespace App\Services\Product;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Traits\HandleImage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductService
{
    use HandleImage;

    protected $productRepository;
    protected $categoryRepository;

    public function __construct(ProductRepositoryInterface $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function getFormData(Request $request)
    {
        return [
            'name' => $request->get('name'),
            'quantity' => $request->get('quantity'),
            'price' => $request->get('price'),
            'description' => $request->get('description'),
            'categories' => $request->get('categories'),
            'image' => $request->file('image')
        ];
    }

    public function index($limit = 5)
    {
        return $this->productRepository->with(['categories'])->latest('id')->paginate($limit);
    }

    public function create(Request $request)
    {
        if ($request->hasFile('image')) {
            $data = $this->getFormData($request);
            $imagePath = $this->storeImage($data['image']);
            if ($imagePath) {
                $product = $this->productRepository->create([
                    'name' => $data['name'],
                    'quantity' => $data['quantity'],
                    'price' => $data['price'],
                    'description' => $data['description'],
                    'image' => $imagePath
                ]);

                $categoriesInt = array_map('intval', $data['categories']);
                $this->productRepository->addCategories($product, $categoriesInt);

                return response()->json([
                    'status' => Response::HTTP_CREATED,
                    'message' => 'Product created'
                ]);
            }

        }

        return response()->json([
            'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'message' => 'error'
        ]);
    }

    public function update(Request $request, $id) {
        $product = $this->productRepository->findOrFail($id);
        $data = $this->getFormData($request);
        if (!$request->hasFile('image')) {
            $imagePath = $product->image;
        } else {
            $imagePath = $this->storeImage($data['image']);
            if ($imagePath) {
                $this->deleteImage($product->image);
            }
        }
        $this->productRepository->update([
            'name' => $data['name'],
            'quantity' => $data['quantity'],
            'price' => $data['price'],
            'description' => $data['description'],
            'image' => $imagePath
        ], $id);

        $this->productRepository->removeCategories($product);
        $categoriesInt = array_map('intval', $data['categories']);
        $this->productRepository->addCategories($product, $categoriesInt);

        return response()->json([
            'status' => Response::HTTP_NO_CONTENT,
            'message' => 'Product updated'
        ]);
    }

    public function getProduct($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function delete($id)
    {
        $product = $this->productRepository->findOrFail($id);
        $this->deleteImage($product->image);
        $this->productRepository->removeCategories($product);
        $this->productRepository->delete($id);

        return response()->json([
            'status' => Response::HTTP_NO_CONTENT,
            'message' => 'Product deleted'
        ]);
    }

    public function search($searchTerm, $limit = 5)
    {
        return $this->productRepository->search($searchTerm, $limit);
    }
}
