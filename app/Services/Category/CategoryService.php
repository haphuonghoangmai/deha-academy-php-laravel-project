<?php

namespace App\Services\Category;

use App\Models\Category;
use App\Repositories\Category\CategoryRepositoryInterface;
use Illuminate\Http\Request;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getAll()
    {
        return $this->categoryRepository->all();
    }

    public function index()
    {
        return $this->categoryRepository->get()->toTree();
    }

    public function getItem($id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function create(Request $request)
    {
        $parentId = $request->get('parent-id');
        if ($parentId) {
            $parent = $this->categoryRepository->find($parentId);
            if ($parent) {
                $node = new Category([
                    'name' => $request->name
                ]);
                $node->appendToNode($parent)->save();
            }
        } else {
            $this->categoryRepository->create([
                'name' => $request->name
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $this->categoryRepository->update([
            'name' => $request->name
        ], $id);
    }

    public function delete($id)
    {
        $category = $this->categoryRepository->findOrFail($id);
        $this->categoryRepository->delete($id);
    }
}
