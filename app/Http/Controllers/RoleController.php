<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function index()
    {
        $roles = $this->role->all();
        return view('roles.index', compact('roles'));
    }

    public function edit($id)
    {
        $role = $this->role->findOrFail($id);
        return view('roles.edit', compact('role'));
    }
}
