<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CategoryRequest;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->index();
        return view('category.index', compact('categories'));
    }

    public function show($id)
    {
        $category = $this->categoryService->getItem($id);
        return view('category.show', compact('category'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(CategoryRequest $request)
    {
        $this->categoryService->create($request);
        return redirect(route('categories.index'));
    }

    public function edit($id)
    {
        $category = $this->categoryService->getItem($id);
        return view('category.edit', compact('category'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $this->categoryService->update($request, $id);
        return redirect(route('categories.edit', $id));
    }

    public function destroy($id)
    {
        $this->categoryService->delete($id);
        return redirect(route('categories.index'));
    }
}
