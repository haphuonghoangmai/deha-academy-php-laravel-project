<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'quantity' => 'required|numeric|digits_between:0,5',
            'price' => 'required|numeric|between:0,9999999',
            'description' => 'required',
            'categories' => 'required|array',
            'image' => ['required', File::image()]
        ];
    }
}
